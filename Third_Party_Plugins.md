---
layout: page
---

# External plugins for RKWard

RKWard can be enhanced by installing additional plugin packages,
sometimes referred to as "external plugins". Some of these plugins come in
format of ordinary R packages and are hosted on our plugin repository
<https://files.kde.org/rkward/R>. These packages usually don't contain
actual R code, but only the plugin code needed to add new functionality
to RKWard.

## Install external plugins

You don't have to download these packages manually, as they can can be
installed via RKWard's package management, just like any other R
package. The package management dialog indicates that a package enhances RKWard, by showing a small
RKWard icon:

![RKWard_R_package_management.png](/assets/img/RKWard_R_package_management.png)

If you check "Include dependencies", like seen above, you will not only
install the plugin package, but all packages that it suggests, too.
These are usually additional R packages which provide functions that the
plugin dialogs depend on. However, if you don't want to install all of
these packages now, you don't have to -- just uncheck the "Include
dependencies" option. You will then be prompted for a package
installation when you actually run a certain dialog of the plugin
package, if it requires something you have not installed yet.

Right after installation of a plugin package, you will usually see a
popup message, informing you that an additional plugin has been found
and added to your configuration. That is, you don't have to configure
anything manually and can just use the new dialogs right away.

## Writing your own plugins

Of course you can not only install additional plugins, but write your
own. Refer to the [Introduction to Writing RKWard Plugins](/doc/rkwardplugins/), which you can also read
locally, it's linked from RKWard's welcome message.
