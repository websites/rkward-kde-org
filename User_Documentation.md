---
layout: page
---

# User Documentation on RKWard

At the time of this writing, the most complete overview of RKWard is
given in an article in the Journal of Statistical Software
([Abstract](http://www.jstatsoft.org/v49/i09),
[PDF](http://www.jstatsoft.org/v49/i09/paper)).

Our [Screenshots](Screenshots.html) and [Mission Statement](RKWard_Overview.html) may also help to get a basic idea
of RKWard, quickly.

## Installing and Troubleshooting

  - [Available Downloads](Download_RKWard.html)
  - [Building RKWard From Source](Building_RKWard_From_Source.html)
  - [General FAQ](General_FAQ.html)

## Further guides on this site

  - [RKWard for Newcomers](RKWard_for_Newcomers.html)
  - [Sample scripts for usage in R/RKWard](RKWard_scripts.html)
  - [List Of Menu Items and Plugins](List_Of_Menu_Items_and_Plugins.html) (out of date and
    incomplete\!)

## Further reading

  - [Developer Information](Developer_Information.html)
