---
layout: page
title: RKWard on macOS
---
{::options toc_levels="2..2" /}

# RKWard on macOS

* Table of contents
{:toc}

## Status

Developer access to Mac computers is still limited for the main contributors. While RKWard is thought to work reasonably well on Mac OS X, we want to encourage you to send feedback to our
[**mailing lists**](Contact.html), be it success stories or bug reports, because without it is hard improve our support for this operating system in the future\!

The 0.8.0 version of RKWard on Mac is known to crash during startup, occasionally, for reasons still under investigation. Please just try again. Once successfully started, RKWard should work, reliably.

## Installing RKWard version 0.8.0

### Requirements

* RKWard 0.8.0 currently requires Mac OS X 10.15 or higher.
* **You have to install R, separately**, version 4.2.0 or higher. Get it from <https://www.r-project.org/>. Make sure to 

### Download latest official version

* .dmg instalatation image of RKWard 0.8.0 can be found at: <https://download.kde.org/stable/rkward/0.8.0/>. Be sure to select the .dmg suitable for your architecture (x86 or arm). **This must match the architecture of your R installation**, even if your system supports both types of binary.

### Installation procedure

**We hope to find a way to make gatekeeper accept RKWard out of the box**. Currently the installation procedure is as follows:

1. Download the installer from on of the links above.
2. Drag the downloaded DMG to your Desktop (or some other location on your harddrive).
3. **Right-click / Ctrl-click on the DMG, and select "Open"**. Drag RKWard to Applications. When warned that "Apple cannot check it for malicious software", click "Open".
4. The first time you start RKWard, you have to start it from Finder: Go to "/Applications", **right-click / ctrl-click on RKWard and select "Open"**. When warned that "Apple cannot check it for malicious software", **again**, click "Open", again.
5. On subsequent runs, you can start RKWard from Launchpad with a regular click.

## Installing development snapshots

Nightly builds of RKWard are available at <https://cdn.kde.org/ci-builds/education/rkward/master/macos/>. These usually have the same requirements as the latest official release.

**These packages are built automatically, and have not received any human testing.** Back up your data before use!

## Older releases that may still be of interest

* RKWard 0.7.3 is the latest version to support OSX 10.13. It is offered in two separate versions, for use with either R 4.1.x or R 4.2.x:
   * For use with R 4.1.x: <https://download.kde.org/stable/rkward/0.7.3/mac/rkward-0.7.3-OSX-10.13-KF5-5.93.0_needs-R-4.1.dmg>
   * For use with R 4.2.x: <https://download.kde.org/stable/rkward/0.7.3/mac/rkward-0.7.3-OSX-10.13-KF5-5.93.0_needs-R-4.2.dmg>

* RKWard 0.7.0 is the last version of RKWard to be built with support for Mac OS X 10.11 or 10.12.
   * <https://download.kde.org/Attic/rkward/0.7.0/mac/RKWard-binary-0.7.0b_OSX10.11_KF5-5.42.0_needs_CRAN_R-3.4.4.pkg>
     (for R 3.4.4)
   * <https://download.kde.org/Attic/rkward/0.7.0/mac/RKWard-binary-0.7.0b_OSX10.11_KF5-5.42.0_needs_CRAN_R-3.5.0.pkg>
     (for R 3.5.0)

### Uninstalling the bundle

To uninstall RKWard, drag the app from your Applications to Trash. An
uninstaller dialog should pop up and offer to remove the app completely
(do a quick check that it offers to remove */opt/rkward*).
Alternatively, you can also run the uninstaller directly from your
personal Applications folder (in your user's home directory).

If you don't want to use the uninstaller, you can also remove RKWard
manually by following the [instructions on uninstalling the MacPorts
source installation](#Uninstalling_RKWard.html).

## Installing from source

Note: RKWard MacOS binaries used to be based on MacPorts. For information on that procedure, refer to an archived version of this page: <http://web.archive.org/web/20210831005515/https://rkward.kde.org/RKWard_on_Mac.html>.

The recommended way to build RKWard and its dependencies on Mac is to use Craft:

1. Installing Craft on Mac: <https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Mac#Installation_using_Craft>
2. Start Craft: `source ~/CraftRoot/craft/craftenv.sh`
3. Get RKWard and all dependencies: `craft -i --target=master rkward` ("--target=master" is optional, and means to build the development version)
4. Create a DMG: `craft --package rkward`
5. Install the DMG, don't forget to install R, separately.

## Troubleshooting

Again: Some detail or other of the above instructions is probably just
wrong. If / when you get an error, please don't give up. Send us a
[mail](Contact.html), and we will talk you through. BTW: If you
figured out the remaining details all by yourself: Great\! But please
drop us a note as well, so we can provide more accurate instructions in
the future.

### RKWard crashes on start

**Possible solution no. 1:** Your KDE setup is probably not ready, most
likely dbus is not running. Before you can start any KDE software, you
first need to run the following commands once: sudo launchctl load -w
/Library/LaunchDaemons/org.freedesktop.dbus-system.plist launchctl load
-w /Library/LaunchAgents/org.freedesktop.dbus-session.plist

**Possible solution no. 2:** You had installed R from another source
before, RKWard picks the wrong one and doesn't find its own R
package(s). If possible, remove the older R installation first.

### Language is always english

**Possible solution:** If the `LANG` variable is not set in the
environment RKWard is started, you will always get the english
interface. To change that, e.g. into german, you can also use
`launchctl`, before you start RKWard: launchctl setenv LANG de_DE.UTF-8
This setting takes effect immediately, but won't survive a reboot. A
more permanent solution is to set this environment variable for your
user account by creating or adding to the
file`~/.MacOSX/environment.plist`:

    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
        <key>LANG</key>
        <string>de_DE.UTF-8</string>
    </dict>
    </plist>
