---
layout: post
title: RKWard 0.6.4 - Usability improvements, fixes, and preparation for porting to KDE frameworks version 5
date: 2015-12-21 00:01:00
---

Shortly before the end of the year, a new version of RKWard is available
for [download](Download_RKWard.html). While the list of changes
looks relatively short this time, beyond the usual fixes, with the help
of usability expert Jan Dittrich, we have addressed some long standing
potential for confusion in several important parts of RKWard's user
interface.

Among the changes that do not quite show in our ChangeLog, we continue
receiving more and more translations from the [KDE language
teams](http://l10n.kde.org). Also we are working to finish our move to
KDE.org. Importantly, we now track new issues on the KDE bugtracking
system (see [Bugs](Bugs.html))). A [lot of
work](https://files.kde.org/rkward/R/pckg/rkwarddev/NEWS.html) went into
the [rkwarddev package](https://github.com/rkward-community/rkwarddev)
to make the plugin generating code much more intuitive. In particular,
see the [documentation on the new *js()*
function](http://api.kde.org/doc/rkwardplugins/rkdev_example.html#rkdev_jscode).

Finally, already looking ahead to forthcoming releases, we are making
[good
progress](https://mail.kde.org/pipermail/rkward-devel/2015-November/004338.html)
on porting RKWard to the more modular, more modern version 5 of the KDE
libraries (aka KF5).

As usual, please don't be shy of [contacting](Contact.html) us
with your feedback, suggestions, and contributions\!

## The changes in detail:

### New features and improvements

- Plugins check for correct object type, in more places, but allow
  to proceed with questionable object selections
- Switch to bugs.kde.org as primary issue tracker
- Workspace browser gains functionality to search / filter objects
  by name, making it easy to find objects, quickly
- Separate globalenv() and other environments in the search path
  more clearly in workspace browser
- Complete rework of <optionset> UI for better usability
- Some usability refinements to "Import CSV" plugin
- Disabled the "Import Data"-dialog, as it was considered too
  confusing. The individual importers are still available from the
  menu, separately.
- For multi-item varslots and valueslots, use separate buttons for
  adding / removing items
- Don't show (useless) index number in mutli-value varslots and
  valueslots
- Make the option to disable / enable editing more visible (in the
  data editor's toolbar)
- Add context menu to HTML viewer window, including the ability to
  open links in a new window
- Remove dependency on KHTMLPart

### Fixes

- Fixed: Plugin variable slots taking more than one object would
  not be highlighted in red while invalid
- Fixed: RKWard package repository would be listed twice on fresh
  installations
- Fix some minor toolbar glitches
- Try harder to honor system locale settings
- Remove obsolete "Export tabular data" plugin (superceded by
  "Export Table / CSV")
- Do not invoke symbol name completion while in the middle (not
  end) of a word
