---
layout: post
title: RKWard 0.6.5 - Much improved preview functionality, and more
date: 2016-04-02 00:01:00
---


Today, we are pleased to announce that again, a new version of RKWard is
available for [download](Download_RKWard.html). This new release
fixes some compilation problems with the upcoming R 3.3.0. However, it
is much more than a "hot fix" release. The most notable area of
improvements have been previews: In addition to the time-tested plot
previews, many RKWard plugins now provide previews of imported data, or
results. Also, we have once again fixed various usability issues in
this, and other areas (details below).

Meanwhile work continues on porting RKWard to version 5 ("frameworks")
of the KDE libraries. The curious will find some first experimental
binaries on our [Release Schedule](Release_Schedule.html) page.

As usual, please don't be shy of [contacting](Contact.html) us
with your feedback, suggestions, and contributions\!

## The changes in detail:

### New features and improvements

- Add option to override CSS file used for the output window
- Added context menu option to search for information on current
  symbol online
- Provide better status feedback when searching / filtering among
  installable packages
- Add access to basic menu items for docked previews
- Move preview controls to the button-column of dialogs
- Add preview to Sort data-, Subset data, and Recode categorical
  data-plugins
- Add preview to Power Analysis plugin
- Add support for adding "htmlwidget"- and "gvis"-class objects to
  the output window via rk.print()
- Add plugins for importing Excel files (.xls and .xlsx, Perl- and
  Java-based)
- Add ability to extract <matrix> values row-wise in plugins
- Add convenience JS-function "makeOption()" for use in plugins
- Add previews for CSV, SPSS, and Stata import plugins
- Allow previews for data, (HTML) output, and custom types of
  previews
- Allow previews to be docked to the dialog window, and dock them,
  by default
- Implicitly save code preview visibility and size (instead of the
  former explicit settings)
- data.frame objects outside of globalenv() can be opened
  read-only in an editor window
- Show a warning screen when invoking plugins from the command
  line (or from clicking an rkward://-link in an external
  application)
- Use package type option "binary" from R 3.1.3 onwards, for
  automatic selection of the appropriate binary package

### Fixes

- When manually adding pluginmaps, load these after, not before
  other pluginmaps, by default
- Fixed: Wizard plugins would be too small, initially
- Fixed: Help Search was not longer working correctly with R 3.2.x
- Fix compilation with R 3.3.x
- Fixed: Numerical (display) precision setting was not honored in
  data editor
- Fix several window activation quirks in "Focus follows mouse"
  mode
- File selectors in "Import XYZ" plugins now filter for standard
  file extensions, by default
