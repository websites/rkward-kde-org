---
layout: page
---

# Installing RKWard on Linux/Unix: Binaries and build instructions

This page provides links to the relevant information pages for the various distributions for which official or inofficial packages are available.

If your distribution is not listed, or does not provide an up-to-date-package, consider [building RKWard from source](Building_RKWard_From_Source.html),
and/or filing a support ticket against your distribution.

* 
{:toc}

## AppImage

Starting with version 0.8.0, RKWard is available in AppImage packaging: Download the package, set the executable bit, and it should run on most current distributions (it is advised to install R, separately).

  - AppImages of offical releases: <https://download.kde.org/stable/rkward/>.
  - Development versions: <https://cdn.kde.org/ci-builds/education/rkward/master/>.

An RKWard plugin to automate the process of downloading a new development AppImage can be found at: <https://github.com/rkward-community/rk.downloadAppImage>.

## Debian

RKWard package in debian: <https://packages.debian.org/sid/rkward>

## Ubuntu

RKWard package in debian: <https://packages.ubuntu.com/search?keywords=rkward&searchon=names&suite=all&section=all>

### Project-provided Ubuntu repositories

**These packages are currently out of date, as the required library backports are not yet available for Ubuntu. We hope to update them, soon, and recommend using the AppImage, in the meantime!**

We provide a variety of automated builds for Ubuntu on our
[Launchpad project page](https://launchpad.net/rkward):

  - If you are using the version R shipped with Ubuntu by default:
      - The latest stable release of RKWard:
        <https://launchpad.net/~rkward-devel/+archive/rkward-stable>
      - Testing snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-devel>
      - Daily development snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-kf5-dailys>
  - If you are using the CRAN version of R:
      - The latest stable release of RKWard:
        <https://launchpad.net/~rkward-devel/+archive/rkward-stable-cran>
      - Testing snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-devel-cran>
      - Daily development snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-kf5-dailys-cran>

## Gentoo

RKWard package in Gentoo:
<https://packages.gentoo.org/package/sci-mathematics/rkward>

## FreeBSD

Information on the FreeBSD port of RKWard is here: <https://www.freshports.org/math/rkward-kde/>

## SUSE / openSUSE

<https://software.opensuse.org/package/rkward5>

Current development version at the time of this writing:
<https://build.opensuse.org/package/show/home:vojtaeus/rkward>

## Fedora

<https://src.fedoraproject.org/rpms/rkward>

## ArchLinux

- Stable release: <https://archlinux.org/packages/community/x86_64/rkward/>
