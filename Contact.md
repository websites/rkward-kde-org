---
layout: page
---

# Getting in contact

## Reporting issues

* [Reporting Bugs and issues](Bugs.html)

## Generic questions, feedback and discussion

### Mailing Lists

The main place for questions and discussion about RKWard is our two mailing lists::

* [rkward-users](https://mail.kde.org/mailman/listinfo/rkward-users) The main list for user discussion about RKWard. This is a good place to ask for help with RKWard. Also this mailing lists is kept posted on the most important developments and new file releases.
* [rkward-devel](https://mail.kde.org/mailman/listinfo/rkward-devel) The main list for discussions concerning the development of RKWard. Posting is not restricted to developers. If your topic goes beyond mere usage questions, this list is for you.

Due to problems with spam, posting is restricted to subscribers on both lists, but non-spam posts by non-subscribers will be allowed manually.

One further mailing list receives notifications of new source commits, and buildbot failures. High volume, intended primarily for developers:
* [rkward-tracker](https://mail.kde.org/mailman/listinfo/rkward-tracker) Commit messages and other automated noise.

### Social media

* [RKWard on Mastodon](https://fosstodon.org/@rkward)
