---
layout: page
---

{:toc}

## Official releases

### Source packages

The latest official source packages. See [Building RKWard From Source](Building_RKWard_From_Source.html) for help on compiling.

- **[rkward-0.8.0](https://download.kde.org/stable/rkward/0.8.0/rkward-0.8.0.tar.gz)** (released July 28, 2024)
- [ChangeLog](https://invent.kde.org/education/rkward/-/blob/releases/0.8.0/ChangeLog)

### Compiled binary packages / build scripts

- Linux / Unix / BSD: [Binaries and Build Scripts](Binaries_and_Build_Scripts.html)
- Windows: [RKWard on Windows](RKWard_on_Windows.html)
- MacOS: [RKWard on Mac](RKWard_on_Mac.html)

## Development and testing

The **latest sources** can always the found in the [RKWard Source repository](RKWard_Source_Repository.html).

**Automated builds**, directly from the development sources, are available at <https://cdn.kde.org/ci-builds/education/rkward/master/>.
Note that these have undergone *absolutely no testing* before publication. Use at your own risk. For further information on
using these packages and/or additional options, refer to the platform specific links, above.

We'd like to encourage you to give these a try, and [report](Contact.html) issues. The [Release Schedule](Release_Schedule.html) may contain
further hints on when development changes will become available in an offical realease.

## Additional plugins

RKWard can be enhanced by installing [additional plugin packages](Third_Party_Plugins.html). They come in format of
ordinary R packages and are hosted on our plugin repository.

## Archive

Historical source releases are available from <https://download.kde.org/Attic/rkward/>, and - for even older releses - <http://sourceforge.net/projects/rkward/files/Historical_Releases/>.
