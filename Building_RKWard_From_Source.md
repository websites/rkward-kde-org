---
layout: page
---

# Building RKWard from Source

**Note**: These instructions are targetted at Linux/FreeBSD, where most required tools for compilation from source are readily available. Instructions for [building on Windows](RKWard_on_Windows.html), and [building on MacOS](RKWard_on_Mac.html), are available on the respective platform specific pages.

## Getting the source code

Refer to [Available source packages](Download_RKWard.html) **or** [Getting the source from git](RKWard_Source_Repository.html).

## Installing required dependencies

RKWard requires:

  - KF5-libraries and headers (\>= 5.44) (http://www.kde.org)
  - Qt-libraries and headers (\>= 5.9) (http://www.qt.io)
  - R and headers (http://www.r-project.org)
      - If you compile R from source, make sure to use the "--enable-R-shlib" option to configure.
  - CMake (http://cmake.org)

Finding the appropriate naming of the required packages to install is the hardest part. Often it is easiest to look at the list of build dependencies for older versions of rkward contained in your distribution. Don't shy away from [asking for help](Contact.html).

At the time of this writing, on Debian based systems (such as Ubuntu) the following will get all the dependencies installed (and hopefully the package names will be similar on other distributions):

`apt-get install r-base-dev cmake extra-cmake-modules libkf5archive-dev libkf5config-dev libkf5coreaddons-dev libkf5crash-dev libkf5doctools-dev libkf5i18n-dev libkf5notifications-dev              libkf5parts-dev libkf5texteditor-dev libkf5xmlgui-dev libkf5widgetsaddons-dev libkf5windowsystem-dev qtwebengine5-dev qtbase5-dev qtscript5-dev qtdeclarative5-dev r-base-dev`

On Arch linux:
`pacman -S qt5-webengine qt5-script qt5-declarative ktexteditor r extra-cmake-modules kdoctools`

(Consider contributing the corresponding list for your distribution of choice as a [merge request for this website](https://invent.kde.org/websites/rkward-kde-org)).

## Compilation

IMPORTANT: You will need to have the R shared library installed. This is
typically located in a place like /usr/lib/R/lib/libR.so. Unfortunately,
some distributions do not yet ship their R binary with the shared
library. In those (rare) cases, you'll have to compile and install R on
your own with the option --enable-R-shlib.

Suppose the source is obtained as **rkward-0.7.2.tar.gz** (here, version 0.7.2 is used
as an example, change it accordingly). Then, the recommended steps to build from source are:
```
tar xzf rkward-0.7.2.tar.gz    # extracts to "rkward-0.7.0" directory
cd rkward-0.7.2
mkdir build                    # dedicated build directory is recommended, but not required
cd build
cmake .. [options]             # see *CMake options* below
make -j 5                      # the j parameter specifies the number of parallel units to compile;
                               # on systems with many cores, a higher value will speed up the build
sudo make install
```

### CMake options (configuring)

The first paramter to cmake is the path to the rkward sources. In the example above,
the build directory was created inside the extracted source directory,
CMakeLists.txt exists in the directory immediately above; hence ".."
is the appropriate path in this setup.

A number of parameters may be specified to control the build
process. Some commonly needed are:

  - `-DCMAKE_INSTALL_PREFIX`
      - set the installation base directory. Generally
        ```-DCMAKE_INSTALL_PREFIX=`kde5-config --prefix` ``` should be
        correct.
  - `-DCMAKE_BUILD_TYPE`
      - type of build. Useful settings include
        `-DCMAKE_BUILD_TYPE=Release` and `-DCMAKE_BUILD_TYPE=debugfull`

In some cases you may want to set the following options:

  - `-DR_EXECUTABLE`
      - path to the R executable to build for. You may want to set this, if you have
        multiple versions of R installed, or R is not in your path.
        Example: `-DR_EXECUTABLE=/usr/bin/R`
  - `-DR_HOME`
      - R home directory. Almost certainly this option is not needed.
  - `-DR_INCLUDEDIR`
      - path to R include files. Almost certainly this option is not
        needed.

Further generic options are listed on
<http://www.cmake.org/Wiki/CMake_Useful_Variables> .

### Installation

As a last step, you _have to_ run `make install`. Otherwise RKWard
will be missing important menu-options, may behave strangely, or may not
run at all. As this will require admin permissions, use: `sudo make install`

Make sure you have set the correct path in cmake (the `-DCMAKE_INSTALL_PREFIX` option).

## Running

For now it's recommended to run RKWard from the command-line for
debug-output. For the few available command-line options see

`rkward --help`

## Troubleshooting

The most common source of problems are

1.  Missing some of the build requirements
2.  Wrong / missing parameters to cmake

So please re-read the corresponding sections, first. If you're still in
trouble, [let us know](Contact.html), we'll be happy to help you.

For some systems,
[Binaries_and_Build_Scripts](Binaries_and_Build_Scripts.html)
has more information.
