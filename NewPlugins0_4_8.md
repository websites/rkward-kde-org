---
layout: page
---

# Plugins introdoced or reworked in RKWard 0.4.8

### Plots

  - Barplot
      - Status: Complete
      - Responsible: tfry
      - Tested by (when): tfry / 2007-09-17
      - Notes: Modified to use embeddable common functionality

<!-- end list -->

  - Piechart
      - Status: Complete
      - Tested by (when): tfry / 2007-09-17
      - Responsible: nono_231

<!-- end list -->

  - Dot chart
      - Status: Basic functionality complete
      - Responsible: tfry
      - Tested by (when): sjar / 2007-09-19

### Distributions

  - All distribution plugins (except for the tests)
      - Status: Complete
      - Preliminary Review: tfry / 2007-05-09
      - Responsible: tfry
      - Notes: Reworked to factor out common elements using includes and
        snippets. Distribution plots now all support a grid (under plot
        options).

### X11

### Analysis

  - Crosstabs (N to 1)
      - Status: Complete
      - Responsible: nono_231
      - Tested by (when): tfry / 2007-09-17
      - TODO: a better idea on naming?

<!-- end list -->

  - Crosstabs (N to N)
      - Status: Complete
      - Responsible: nono_231
      - Tested by (when): tfry / 2007-09-17
      - TODO: a better idea on naming?

<!-- end list -->

  - Timeseries Box test
      - Status: Complete
      - Responsible: sjar
      - Tested by (when): tfry / 2007-09-20

<!-- end list -->

  - Timeseries KPSS test
      - Status: Complete
      - Responsible: sjar
      - Tested by (when): tfry / 2007-09-20

<!-- end list -->

  - Linear regression
      - Status: Basic functionality complete
      - Responsible: sjar / nono_231
      - Tested by (when): tfry / 2007-09-24
