---
layout: page
---

# Development news archive

This page is merely an archive of development news from the years 2005
through 2008. Provided for historical purposes. Links may or not be functional.

## 2008

### First preview of RKWard 0.5.0 for KDE 4 - [User:Tfry](User:Tfry.html) - 03 January 2008 -

KDE 4.0 is scheduled to be launched on January 18 2008. The new KDE will
offer some exciting new possiblities for RKWard as well, including the
real possiblity for a port to the Windows operating system.

After considerable porting efforts, I am proud to announce the first
preview release of the upcoming RKWard 0.5.0 for KDE 4. If you are
curious, or want to help with testing, please download from
<http://rkward.sourceforge.net/temp/rkward-0.5.0pre1.tar.gz>. Your
feedback is welcome on the mailing list.

If all goes well, RKWard 0.5.0 will be officially released on January
21st.

## 2006

### RKWard moves to Subversion - [User:Tfry](User:Tfry.html) - 19 September 2006 -

Effective today, RKWard no longer uses CVS, but switched to Subversion
for managing the code base. The entire revision history has been
converted, and everything seems to work. Instructions on how to access
the subversion repository are available
[here](https://sourceforge.net/svn/?group_id=50231). Developers will
need to check out a fresh working copy from the new repository.
Fortunately, most cvs commands also work in SVN without the need to
readjust. Sourceforge provides some more documentation on using
Subversion".

Perhaps most importantly, Subversion allows us to reorganize some
aspects of the source file structure more easily, and this will be done
over the next few days.

### Update on state of development - [User:Tfry](User:Tfry.html) - 24 March 2006 -

Yes, there is progess on development to report. The rework of the plugin
infrastructure, as announced in the last news item, is finally
approaching completion. Most of what is left to do is cleaning up some
things, do some more testing, and fix any bugs found. However, things
are looking good right now, and if previously we told you to hold off on
CVS version, here's an explicit encouragement to fetch the latest CVS
sources and do some bug testing (esp. on the plugins). Updated
documentation on how to create plugins is available here (if you've
developed plugins, previously: not much has changed for existing
plugins, but be sure to read chapters 6 and 7).

Of course, also a new release is getting closer. Until then, however,
some more cleanups have to be done, and some assorted small bugs need to
be fixed. You can help us by starting to do testing on the CVS version
today, and reporting any bugs you find on the development mailing list.

## 2005

### Current development, state of CVS - [User:Tfry](User:Tfry.html) - 24 November 2005 -

This is to let you know a major piece of development is about to start:
Finally the plugin infrastructure will see a rework. Most importantly,
in the future, plugins/components will be able to embed each other,
thereby making it easy to reuse existing functionality in new plugins.
Along the way, also some changes will be done to make it easier to
provide support for "external" plugins, written e.g. in pyQt in future
releases (this is somewhat more long-term, however, and will likely not
be part of rkward 0.3.5)

The planned changes will require some major restructuring in the
plugin-related source files. Likely the CVS version of rkward will be
very buggy for quite some time to come, and probably sometimes will not
even compile. Today's (still functional) state of CVS has been tagged
"date_2005_11_2004_before_component_work", so using the option "-r
date_2005_11_2004_before_component_work" you can retrieve a
functional copy of CVS meanwhile.

### 0.3.4 pre-release / Call for testing - [User:Tfry](User:Tfry.html) - On the 25 October 2005 -

After the last version of RKWard was shipped with a rather grave bug, I
didn't want to wait too long before releasing the next version. Also,
the current list of changes since RKWard 0.3.3 does not look bad at all,
so I think time has come for a new release.

You can download a pre-release here \[link/file removed after the
official release of 0.3.4\]. If you can find the time, please give it a
thorough testing. Tell us about bugs or missing features on the mailing
list, or submit reports to the respective trackers. Thanks\!

### Developers' note for RKWard 0.3.3 - [User:Tfry](User:Tfry.html) - 06 October 2005 -

While I will not reiterate the last news item, or paste in the complete
ChangeLog at this place, I'd like to point out one change that affects
writing plugins. Here, a first step has been done to rework the plugin
infrastructure (more significant changes are planned for future
releases). The plugins shipped with the release have been adapted to
those changes. However, before writing new plugins, you should read the
updated version of section A of the introduction to writing plugins.

### New version soon to be released - [User:Tfry](User:Tfry.html) - 25 September 2005 -

This is a call for testing on the current CVS version. Over the past few
weeks, I've been working on a number of different areas in RKWard. Here
are the most recent ChangeLog entries:

  - reworked handling of output from R (no more file-sinks)
  - fix display of warning messages (behavior does not match plain R,
    yet, but is mostly sane)
  - option to change list of package repositories
  - option to archive downloaded packages
  - assorted bugfixes
  - provide context/function help for the console
  - output-window has been switched to KPart-infrastructure
  - command-editor has been switched to KPart-infrastructure completely
  - data-editor uses KPart-infrastructure (better integration)
  - override standard callbacks used by R for ReadConsole, ShowFiles,
    EditFile(s), ChooseFile, Suicide, CleanUp. No more reliance on stdin
  - some cleanups of dead code
  - plugins can now be placed in different menus
  - keep list of components/plugins and menu-hierarchy in (few)
    .pluginmap files instead of parsing directory structure

Esp. the reworked handling of R-output, and the removals of dead code
may have broken some features or introduced new bugs. Therefore, please
check out the current CVS, and report any bugs, and wishlist items you
come across.

[Category:Developer
Information](Category:Developer_Information.html)
