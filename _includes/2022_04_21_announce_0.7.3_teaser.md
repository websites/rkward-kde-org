### RKWard 0.7.3

*21 April 2022*

A new release, RKWard 0.7.3, is available for [download](Download_RKWard.html), today. 
This release brings support for the new R graphics features, a rework of RKWard output
files, an integrated dialog to ask about all changed files at once on exit, and many
other small improvments all over the place.

