As usual, we're looking forward to your [feedback](Contact.html) suggestions, and contributions!

#### The changes in detail:

  - New features and improvements
     - Added: Partial completions (Tab-key) consider completion candidates from all visible completion groups
     - Added: R's dynamic completions (importantly for ":::", "?", and "@") are merged into the already provided completions
     - Added: Add option to offer code completion/hinting in all file types not just R scripts (e.g. in .Rmd files)
     - Changed default behavior (new installations, only): Up/down without alt navigate completion items if visible in console/editor
     - Added: Provide tooltips on symbols in scripts and R console
     - Added: Many new basic and advanced R, R Markdown and LaTeX snippets, including complete R Markdown templates
     - Added: Allow to select search provider, when searching for a term online
     - Added: Allow to restart R backend (e.g. for testing that scripts or packages will work in a fresh session)
     - Changed: Actions to restart the R backend, interrupt all commands and configure the R backend arranged in a hmburger menu
     - Added: Crosstabs N to N: Simplify labels, add option to control table layout
     - Added: Change mechanism for detection of object changes
  - Bug fixes
     - Fixed: Backend failed to start when installed in a path with spaces on Windows volumes without 8.3 support
     - Fixed: Trying to restart backend could cause a hang, on Windows
     - Fixed: In corner cases, cancelling commands could lead to a lockup
     - Fixed: IRT Cronbach's Alpha did not work for subsets, if the data.frame name contains dots
     - Fixed: Action to remove several rows in data editor, simultaneously, always remained disabled
     - Fixed: Workspace browser would not always show change, immediately, when object type changes
     - Fixed: Crash when using the "Git blame" kate plugin
     - Fixed: Problem installing R support package in some configurations
     - Fixed: Menubar would disapper after opening script editor, in some configurations
     - Fixed: Very long error messages during R markdown preview could cause the preview window to become too wide
     - Fixed: Expresssions spanning several lines would not be shown, correctly, in "R Console"-mode script preview
     - Fixed: Fix focus problems, and better efficiency for data previews (as used in data import dialogs)
     - Fixed: Excel import plugin failed to accept file name
     - Fixed: Fix zooming help/output pages with Ctrl+scroll wheel, when compiled with QWebEngine
     - Fixed: Fix problem handling rkward:// links from dialogs on some sytems
     - Fixed: Fix object name completion for (irregular) names starting with numbers or underscores
