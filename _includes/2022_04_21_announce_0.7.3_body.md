As usual, we're looking forward to your [feedback](Contact.html) suggestions, and contributions!

**Note:** The RKWard 0.7.3 sources are compatible with a wide range of R releases, including (but not limited to) R 4.1.x and R 4.2.x.
However, an RKWard binary compiled against R 4.1.x is not going to work with R 4.2.x, and vice versa. Windows and MacOS binaries are
provided in two different variants, accordingly. Linux users upgrading to R 4.2.0, please contact your distribution maintainers, if needed.

#### The changes in detail:

  - New features and improvements
     - Add option to show standardized coefficients for linear regression
     - Add setting to allow hiding R Console scrollbar minimap
     - "Spontaneous" output, such as from running examples in the enhanced help system in R 4.2.0, is shown in the R Console
     - Support for switching color schemes, including basic support for dark mode
     - Space-saving placement of main window status bar, and display more tips in it
     - Implement R 4.2 graphics functions: stroked and filled paths, groups, lumincance masks
     - Implement R 4.1 graphics functions: gradients, patterns, clip paths, masks
     - Add icons to settings dialog for quick visual orientation
     - Merge ktexteditor (script) settings into the main settings dialog
     - Internal: Code cleanup around settings, and in many other places
     - On exit, ask for saving all unsaved modified files at once
     - Output file handling has been reworked, entirely
     - rkwardtests library gains helper functions for checking for expected errors
     - Internal: Allow R-level calls to support both subcommands, and a return value at the same time
     - Hide or remove several purely internal functions (most can still be assessed from the rkward namespace as rkward:::xyz())
     - Use top alignment button to close (warning) messages in preview windows/areas
     - (Try to) automatically create custom R library locations, if they do not exist
     - kate plugin related actions are now active whenever a script window is active (not only the corresponding tool window)
  - Bug fixes
     - Fixed: Possible backend hang when closing a data editor before it was done fetching its data
     - Fixed: Crash when installing packages from within check installation dialog
     - Fixed: Spurious string I18N_ARGUMENT_MISMATCH in several plugins
     - Fixed: Accordeon control (used in "Recode categorical data") never expanded
     - Fix compilation with the upcoming R 4.2.0
     - Fix crash in dev.capture()
     - Fix plot window not showing when created attached
     - Fixed: Intial graphics window size would be (mostly) ignored
     - Fix some buglets around closing on-screen devices
     - Fixed: Cursor navigation in completion list
     - Fixed: Calling (rk.)select.list() without a title would fail
     - Workaround for invalid EDITOR variable set by Kate Terminal plugin (also fixed in recent kate)
     - Fixed: Crash when attempting to use new graphics features in R 4.1.0 (esp. plotting using ggplot2)
