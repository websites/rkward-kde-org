### RKWard 0.7.0 - Split views, based on KF5

*16 April 2018*

It took much longer than we had hoped for, but today, we proudly
announce that version 0.7.0 of RKWard is available for
[download](Download_RKWard.html). This is the first release of
RKWard to be based on KF5 (version 5 of the KDE libraries), which means
RKWard can again profit from the ongoing development of an actively
supported platform. However, not all changes are behind the curtains.
The most prominent addition in this release is "split view"
functionality, which allows you to partition the main window for viewing
your scripts or data side-by-side. Beyond that a number of bugs were
fixed, and we polished our installers for Windows and Mac.

**Update**: Translations were missing from installations of 0.7.0. This
problem has been addressed in version 0.7.0b of RKWard.
