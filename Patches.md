---
layout: page
---

# Contributing source and documentation patches

You have found a problem **and** a way to fix it (i.e. "a patch")? Then this page will show you how to contribute your patch.

## Contributing for the impatient

You're trying to help? Great! And if you don't feel like reading / following the instructions below, that's ok. If you're looking for the fastest way to contribute, then just contact us on the [mailing list](Contact.html).

However one piece of advise: Please try to be as specific as possible. Importantly:
  - When sending us a patch, please create it using ```git diff > mypatch.diff```.
  - If the underlying issue is not yet reported, please take some time to describe it in detail. Follow the same guidlines as for [creating a bug report](Bugs.html).
  - If you are unsure about parts of your patch (or you have not really tested it): That's nothing to be ashamed of, but please let us know!

## The expert way to contributing patches: Gitlab merge requests
As always with git, there are many way to accomplish something. If you are a git guru and want to do things differently, by all means do. If you just want a recipe to follow, here you go:

### Initial steps
  1. Get a [KDE identity account](https://identity.kde.org). In the following, we'll assume your account name is *USER*.
  2. Find to the correct repository on invent.kde.org:
     - [The main RKWard repository](https://invent.kde.org/education/rkward)
     - [The RKWard website repository](https://invent.kde.org/websites/rkward-kde-org)
  3. Fork the repository by clicking "Fork" on the repository web page (or the website repo, see above).
  4. Clone the (original) repository to your local disk (assuming you haven't, yet):

         `git clone https://invent.kde.org/education/rkward.git`

  5. Add your fork to your local clone ("USER" is just a label, here; you can pick your KDE account name, or some other label to tell you that this is your fork):

        `git remote add USER https://invent.kde.org/USER/rkward.git`

### Starting a (patch and) merge request
  6. For good measure, before you start, do a ```git checkout master; git pull``` in your clone. To make sure you're up to date with the most recent commits. In case of conflicts, ```git stash; git pull; git stash pop``` will usually save you.
  7. Create a "feature branch" for your patch:

        `git checkout -b MYPATCH`

  8. Create and test your patch (it's ok, if you did this step before the previous one, as long did not commit anything, yet)
  9. Once satisfied commmit your patch, locally (usually: ```git commit -a```; check your work with ```git status```), then

        `git push --set-upstream USER MYPATCH`

  10. Go to the webpage of your fork. You should see your new commit, there, along with a button to create a merge request. Do that.

### Updating your patch
  11. Make sure you are on your already existing feature branch: ```git checkout MYPATCH```
  12. Edit and test your patch, commit the result, locally
  13. Pushing to your fork now becomes a little easier, as git already knows where to push the branch:

         `git push`

  14. The merge request is updated, automatically. For good measure navigate to <https://invent.kde.org/education/rkward> verify everything went as expected, and probably to leave a comment explaining your changes.

### Further reading
This workflow is explained in some more detail at <https://community.kde.org/Infrastructure/GitLab>.
