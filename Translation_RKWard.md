---
layout: page
---

# Getting started translating RKWard

## General info

The primary ressource for RKWard translations is <https://l10n.kde.org/>.
A lot of documenation, and contact info for your language team is
available, there.

Note that the translation files are divided into the strings for the (core) application, and
several separate catalogs for plugins and help pages. For translating the latter, refer to <https://rkward.kde.org/doc/rkwardplugins/i18n.html>.

## Testing your translation

To test your translation, copy the .po file you created/modified to the
"i18n"-subdirectory of your rkward sources. Then do cmake, make, make
install as detailed in the INSTALL instructions.

*Use Settings->Switch Application Language* to switch between languages. On most systems you can also use environment variables to change the language of rkward, i.e:

```LANG=xx rkward```

(where xx would be your language code, e.g. fr).

On some systems it appears to be necessary to specify *both* KDE_LANG
and LC_ALL. E.g.

```LC_ALL=de_DE.utf-8 KDE_LANG=de rkward```

## Developers: Maintainance of translations

Synchronize translations from KDE l10n to git like this:

  - ```scripts/import_translations.py```
  - ```scripts/export_translations.py```
